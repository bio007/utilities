# Repository containing utility libraries for common use cases in Android development

## EmptyStateRecyclerView
RecyclerView with support for empty state view.

Use the library adding `implementation 'io.github.two-coders:emptystaterecyclerview:1.0.2'` into your build.gradle file.

## DynamicText
> ### Migration notice
> **Note that this project is moved to GitHub [android-dynamic-componets](https://github.com/Two-Coders/android-dynamic-componets) repository** and is not maintained here anymore.
> 
> To migrate, change `com.kacera:dynamictext:2.x` to `com.twocoders.dynamic:color:3.x`

Data class which can be used to combine String data with string resource identifiers. Can be used for regular strings or plurals.

Use the library adding `implementation 'com.kacera:dynamictext:2.1.1'` into your build.gradle file.

### Usage
One of the usage cases this comes handy is when you want to conditionally set either String text or StringRes id to one TextView using data-binding.

Code in ViewModel
```
val title = MutableLiveData<DynamicText>()

fun onDataLoaded(response: Response) {
    if (response.success) {
        title.value = DynamicText.from(response.data.title)	//data.title is String
    } else {
        title.value = DynamicText.from(R.string.oh_no)	//id of a string from resources is Int
    }
}
```

Code in layout file
```
<TextView
    ...
    android:text="@{viewModel.title}"/>
```