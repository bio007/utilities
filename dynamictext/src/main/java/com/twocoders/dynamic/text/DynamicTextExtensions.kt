package com.twocoders.dynamic.text

import androidx.annotation.Keep

@Keep
fun String?.toDynamicText() = this?.let { DynamicText.from(it) } ?: DynamicText.EMPTY